class AddAgentIdToResource < ActiveRecord::Migration[5.2]
  def change
    add_column :resources, :agent_id, :bigint, limit: 8
    add_index  :resources, :agent_id
  end
end
