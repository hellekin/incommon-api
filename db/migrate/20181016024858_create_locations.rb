class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.references :position, foreign_key: true
      t.references :locatable, polymorphic: true

      t.timestamps
    end
  end
end
