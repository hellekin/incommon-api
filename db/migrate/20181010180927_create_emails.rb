class CreateEmails < ActiveRecord::Migration[5.2]
  def change
    create_table :emails do |t|
      t.string :address
      t.string :sha256_hash, limit: 40
      t.integer :flags

      t.timestamps
    end
  end
end
