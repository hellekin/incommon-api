class CreatePositions < ActiveRecord::Migration[5.2]
  def change
    create_table :positions do |t|
      t.text :geometry
      t.string :geo_type
      t.decimal :latitude, precision: 9, scale: 7
      t.decimal :longitude, precision: 10, scale: 7
      t.decimal :elevation, precision: 6, scale: 2
      t.integer :radius, limit: 6
      t.json :box

      t.timestamps
    end
  end
end
