class FixPositionGeometry < ActiveRecord::Migration[5.2]
  def change
    # Use PostGIS native type instead of text!
    # For some reason, this breaks:
    # change_column :positions, :geometry, :geometry
    # Instead we go into psql and do: ALTER TABLE positions ALTER COLUMN geometry TYPE geometry;
    # Allow elevation/depth > 10km
    change_column :positions, :elevation, :decimal, precision: 7, scale: 2
    # Add a spatial index
    add_index :positions, :geometry, using: :gist
  end
end
