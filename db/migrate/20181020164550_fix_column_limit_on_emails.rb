class FixColumnLimitOnEmails < ActiveRecord::Migration[5.2]
  def change
    change_column :emails, :sha256_hash, :string, limit: 64, index: true
  end
end
