class CreateResourcePhones < ActiveRecord::Migration[5.2]
  def change
    create_table :resource_phones do |t|
      t.references :resource, polymorphic: true
      t.references :phone, foreign_key: true

      t.timestamps
    end
  end
end
