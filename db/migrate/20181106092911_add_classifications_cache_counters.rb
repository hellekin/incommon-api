class AddClassificationsCacheCounters < ActiveRecord::Migration[5.2]
  def change
    add_column :taxonomies, :categories_count, :integer, default: 0
    add_column :categories, :sections_count,   :integer, default: 0
    add_column :sections,   :resources_count,  :integer, default: 0
  end
end
