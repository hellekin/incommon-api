class FixFlagsDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :emails, :flags, :integer, default: 0, null: false
    change_column :links, :flags, :integer, default: 0, null: false
    change_column :phones, :flags, :integer, default: 0, null: false
    change_column :roles, :roles, :integer, default: 0, null: false
    change_column :resource_addresses, :flags, :integer, default: 0, null: false
    change_column :resource_emails, :flags, :integer, default: 0, null: false
    change_column :resource_links, :flags, :integer, default: 0, null: false
    change_column :resource_phones, :flags, :integer, default: 0, null: false
  end
end
