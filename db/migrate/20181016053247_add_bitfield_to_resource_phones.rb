class AddBitfieldToResourcePhones < ActiveRecord::Migration[5.2]
  def change
    add_column :resource_phones, :flags, :integer
  end
end
