class AddRankToClassifications < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :rank, :integer
    add_column :sections, :rank, :integer
  end
end
