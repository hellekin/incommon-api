class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, limit: 64, unique: true
      t.string :password_digest
      t.uuid   :api_token, unique: true
      t.uuid   :uuid, unique: true
      t.string :auth_token, unique: true
      t.string :token     

      t.timestamps
    end
  end
end
