class CreateTaxonomies < ActiveRecord::Migration[5.2]
  def change
    create_table :taxonomies do |t|
      t.string :name, limit: 64
      t.string :summary, limit: 136
      t.text :description
      t.string :uuid, limit: 36

      t.timestamps
    end
    add_index :taxonomies, :name, unique: true
    add_index :taxonomies, :uuid, unique: true
  end
end
