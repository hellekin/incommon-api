class AddResourcesSectionsJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :resources, :sections do |t|
      t.references :resource, foreign_key: true
      t.references :section, foreign_key: true
    end
  end
end
