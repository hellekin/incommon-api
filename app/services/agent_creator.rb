# frozen_string_literal: true

class AgentCreator
  attr_reader :agent, :creator,
              :email, :name, :summary, :description, :uri,
              :email_validated, :uri_is_origin

  def initialize(options)
    Rails.logger.info "initialize #{options.inspect}"
    @email           = options[:email] || raise(ArgumentError, 'Email required')
    @name            = options[:name]  || raise(ArgumentError, 'Name required')
    @summary         = options[:summary]
    @description     = options[:description]
    @uri             = options[:uri]
    @email_validated = (options[:email_validated]) || false
    @uri_is_origin   = (options[:uri_is_origin]) || false
    @agent           = nil
  end

  def self.create(options)
    Agent.transaction do
      @creator = new(options)
      Rails.logger.info "AgentCreator.create(name: #{@creator.name}, email: #{@creator.email}...)"
      Rails.logger.info "In transaction with #{@creator.name}, #{@creator.email}"
      agent = Agent.i18n.find_or_create_by!(
        name: @creator.name,
        summary: @creator.summary,
        description: @creator.description
      )
      if agent.id_before_last_save.nil?
        # New record: needs email & link
        Rails.logger.info "Yes, a new user! #{@creator.inspect} #{agent.inspect}"
        email_for!(agent)
        link_for!(agent) if @creator.uri.present?
        agent.save
      end
      @agent = agent if agent.valid?
    end
  end

  private
  
  def self.email_for!(agent)
    Rails.logger.info "Setting email to #{@creator.email} for #{agent.name}"
    email        = Email.with_address!(@creator.email)
    re           = agent.resource_emails.create!(email: email)
    re.primary   = true
    re.validated = @creator.email_validated
    re.save
  end

  def self.link_for!(agent)
    Rails.logger.info "Setting link to #{@creator.uri} for #{agent.name}"
    rl          = agent.resource_links.create!(link: Link.with_uri!(@creator.uri))
    rl.homepage = true
    rl.origin   = @creator.uri_is_origin
    rl.save
  end
end
