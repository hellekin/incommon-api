# frozen_string_literal: true

class Api::V0::RolesController < ApplicationController
  def index
    @roles = Role.all
    render jsonapi: @roles,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @role = Role.find(params[:id])
    render jsonapi: @role,
           include: include_from_params,
           fields: params[:fields]
  end

  private

  def model_types
    %w(agent user).freeze
  end
end
