# frozen_string_literal: true

class Api::V0::CollectionsController < ApplicationController
  def index
    @collections = policy_scope(Collection)
    render jsonapi: @collections,
           include: include_from_params
  end

  def show
    @collection = policy_scope(Collection).find(params[:id])
    authorize @collection
    render jsonapi: @collection,
           include: include_from_params
  end

  private

  def model_types
    %w(agent maps resource_collections resources).freeze
  end
end
