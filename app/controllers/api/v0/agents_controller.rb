# frozen_string_literal: true

class Api::V0::AgentsController < ApplicationController
  def index
    @agents = policy_scope(Agent)
    render jsonapi: @agents,
           fields: params[:fields],
           include: include_from_params
  end

  def show
    @agent = policy_scope(Agent).find_by(uuid: params[:id])
    authorize @agent
    render jsonapi: @agent,
           include: include_from_params,
           fields: params[:fields]
  end

  private

  def model_types
    %w(collections editors leaders maintainers observers origins roles sections taxonomies users).freeze
  end
end
