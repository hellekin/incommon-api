# frozen_string_literal: true

class Api::V0::MapsController < ApplicationController
  def index
    @maps = policy_scope(Map).includes(:collection).all
    render jsonapi: @maps,
           include: include_from_params
  end

  def show
    @map = Map.includes(:collection, :taxonomy).find_by(uuid: params[:id])
    render jsonapi: @map,
           include: include_from_params
  end

  private

  def model_types
    %w(agent collection resources position
       taxonomy taxonomy.categories taxonomy.sections).freeze
  end
end
