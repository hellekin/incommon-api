# frozen_string_literal: true

class Api::V0::CategoriesController < ApplicationController
  def index
    @categories = Category.all
    render jsonapi: @categories,
           include: include_from_params
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  def show
    @category = Category.find(params[:id])
    render jsonapi: @category,
           include: include_from_params
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  private

  def model_types
    %w(taxonomy sections).freeze
  end
end
