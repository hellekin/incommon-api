# frozen_string_literal: true

class Api::V0::TaxonomiesController < ApplicationController
  def index
    @taxonomies = Taxonomy.all
    render jsonapi: @taxonomies,
           fields: params[:fields],
           include: include_from_params
    # expose: [current_user, current_agent]
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  def show
    @taxonomy = Taxonomy.find_by(uuid: params[:id])
    render jsonapi: @taxonomy,
           include: include_from_params,
           fields: params[:fields],
           links: { agent: api_agent_url(@taxonomy.agent) }
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  private

  def model_types
    %w(agent categories sections).freeze
  end
end
