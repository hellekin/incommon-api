# frozen_string_literal: true

class Api::V0::SectionsController < ApplicationController
  def index
    @sections = Section.all
    render jsonapi: @sections,
           fields: params[:fields],
           include: include_from_params
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  def show
    @section = Section.find(params[:id])
    render jsonapi: @section,
           include: include_from_params,
           fields: params[:fields]
  rescue INCOMMON::API::UnknownRelationshipError => e
    render_jsonapi_error_unknown_relationship(e.message, model_types)
  end

  private

  def model_types
    %w(taxonomy category category.taxonomy resources
       agents entities places services things).freeze
  end
end
