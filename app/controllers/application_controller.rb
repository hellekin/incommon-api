# frozen_string_literal: true

class ApplicationController < ActionController::API
  # INCOMMON::API::Controller implements an ActiveSupport::Concern to handle API
  # authorization and error handling for the whole application.
  # @see app/lib/incommon/api/controller.rb
  include INCOMMON::API::Controller
  # Pundit implements authorization policies.
  # @see https://github.com/varvet/pundit
  include Pundit

  if Rails.env.development?
    # @see https://github.com/varvet/pundit#ensuring-policies-and-scopes-are-used
    after_action :verify_authorized, except: :index,  if: proc { params[:pundit] == 'verify' }
    after_action :verify_policy_scoped, only: :index, if: proc { params[:pundit] == 'verify' }
  end
end
