# frozen_string_literal: true

class SerializablePosition < JSONAPI::Serializable::Resource
  type 'positions'

  attribute :id
  attribute :box
  attribute :elevation
  attribute :geo_type
  attribute :geometry
  attribute :latitude
  attribute :longitude
  attribute :radius

  attribute :created_at
  attribute :updated_at

  belongs_to :agent
  belongs_to :user

  link :self do
    @url_helpers.api_role_url(@object)
  end
end
