# frozen_string_literal: true

class SerializableResourceCollection < JSONAPI::Serializable::Resource
  type 'resource_collections'

  attribute :created_at
  attribute :updated_at

  belongs_to :collection do
    link :self do
      @url_helpers.api_collection_url(@object.collection)
    end
  end
  belongs_to :resources
end
