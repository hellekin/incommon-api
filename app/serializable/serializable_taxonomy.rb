# frozen_string_literal: true

class SerializableTaxonomy < JSONAPI::Serializable::Resource
  type 'taxonomies'

  id { @object.uuid }

  attribute :name
  attribute :summary
  attribute :description
  attribute :created_at
  attribute :updated_at

  meta locale: I18n.locale

  belongs_to :agent
  has_many   :categories do
    meta count: @object.categories_count
  end
  has_many   :sections

  link :self do
    @url_helpers.api_taxonomy_url(@object)
  end
end
