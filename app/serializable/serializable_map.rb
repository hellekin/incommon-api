# frozen_string_literal: true

class SerializableMap < JSONAPI::Serializable::Resource
  type 'maps'

  id { @object.uuid }

  attribute :id
  attribute :zoom
  attribute :center do
    {
      longitude: @object.center[0],
      latitude: @object.center[1],
    }
  end

  # belongs_to :agent
  belongs_to :collection do
    link :self do
      @url_helpers.api_collection_url(@object.collection)
    end
    meta do
      {
        resource_count: @object.collection.resources.count,
      }
    end
  end
  belongs_to :position do
    link :self do
      @url_helpers.api_position_url(@object.position)
    end
  end
  belongs_to :taxonomy

  has_many :resources do
    data do
      @object.resources
    end
  end

  link :self do
    @url_helpers.api_map_url(@object)
  end

  meta do
    { locale: I18n.locale }
  end
end
