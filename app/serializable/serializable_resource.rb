# frozen_string_literal: true

class SerializableResource < SerializableIncommonResource
  type { @object.type }

  id   { @object.uuid }

  # Mobility translations
  attribute :name
  attribute :summary
  attribute :description

  attribute :created_at
  attribute :updated_at

  belongs_to :agent

  link :self do
    @url_helpers.api_resource_url(@object)
  end
end
