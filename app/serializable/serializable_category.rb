# frozen_string_literal: true

class SerializableCategory < JSONAPI::Serializable::Resource
  type 'categories'

  # Mobility :translations
  attribute :name
  attribute :summary
  attribute :description

  attribute :color
  attribute :created_at
  attribute :updated_at

  meta locale: I18n.locale

  belongs_to :taxonomy
  has_many   :sections do
    meta count: @object.sections_count
  end

  link :self do
    @url_helpers.api_category_url(@object)
  end
end
