# frozen_string_literal: true

module INCOMMON
  module API
    module Errors
      class UnknownRelationshipError < ArgumentError; end
      class UnauthorizedError < StandardError; end
    end
  end
end
