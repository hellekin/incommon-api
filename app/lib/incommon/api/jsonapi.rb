# frozen_string_literal: true

module INCOMMON
  module API
    module JSONAPISerializableHelper
      private

      def meta_current
        {
          current: {
            agent_name: @current_user&.name,
            agent_uuid: @current_user&.uuid,
            user_uuid: @current_user&.user&.uuid,
            restricted: @current_user&.user&.restricted?,
            anonymous: @current_user&.user&.anonymous?,
          },
        }
      end

      def redacted
        I18n.t('redacted')
      end

      def anonymous?
        @current_user&.user&.anonymous? == true
      end

      def editor?
        @current_user&.editor? == true
      end

      def leader?
        @current_user&.leader? == true
      end

      def leader_or_me?
        leader? || me?
      end

      def maintainer?
        @current_user&.maintainer? == true
      end

      def me?
        @current_user.presence == @object
      end

      def observer?
        @current_user&.observer? == true
      end

      def restricted?
        @current_user&.user&.restricted? == true
      end
    end
  end
end
