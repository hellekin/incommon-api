# frozen_string_literal: true

module INCOMMON
  module API
    class Migrations < ActiveRecord::Migration[5.2]
      # rubocop:disable Metrics/MethodLength
      def change
        unless User.find(-1)
          # Create INCOMMON::AnonymousUser
          User.create(
            id: -1,
            username: 'anon@incommon.cc',
            api_token: '23d8009f-f2ec-44b6-bcbe-58ad1d3af10b',
            uuid: '6c8be57b-5730-428d-bdf2-ff9bcb37d7fd'
          )
        end
        unless Agent.find(-1)
          # Create INCOMMON::IncommonAgent
          Agent.create(
            id: -1,
            public: true,
            translations: {
              en: {
                name: 'IN COMMON Agent',
                summary: 'Default Agent for the IN COMMON Collective.',
                description: <<-DESCRIPTION
                ## IN COMMON Agent

                When no Agent is available to manage Resources, this Agent is used.

                ### Call Sequence

                ``` ruby
                agent = INCOMMON::IncommonAgent.new
                agent.name # => 'IN COMMON Agent'
                ```

                ### Functionality

                `IncommonAgent` works like any other Agent, except:
                - it is always **not** authenticated: `agent.authenticated? # => false`
                - it is always an editor: `agent.editor? # => true`
                  This allows to create new resources anywhere.
                - it has no other role, e.g.: `agent.maintainer? # => false`
                  This prevents from editing or observing Resources from other Agents,
                  and prevents from changing user Roles.

                If you encounter an issue with `INCOMMON::IncommonAgent`, please report
                it to <https://framagit.org/incommon/incommon-api>.
                DESCRIPTION
              },
              fr: {
                name: 'Agent IN COMMON',
                summary: "L'Agent par défaut pour le collectif IN COMMON.",
                description: <<-DESCRIPTION
                ## Agent IN COMMON

                Lorsqu'aucun Agent n'est disponible pour maintenir des Ressource,
                on utilise cet Agent.

                ### Utilisation

                ``` ruby
                Mobility.locale = :fr
                agent = INCOMMON::IncommonAgent.new
                agent.name # => 'Agent IN COMMON'
                ```

                ### Fonctionalité

                `IncommonAgent` fonctionne comme n'importe quel autre Agent, avec les
                exceptions suivantes :
                - il n'est **jamais** identifié : `agent.authenticated? # => false`
                - il est toujours un éditeur : `agent.editor? # => true`
                  Cela permet de créer de nouvelles Ressources n'importe où.
                - il n'a aucun autre rôle, par exemple : `agent.maintainer? # => false`
                  Cela empêche la modification ou l'observation de Ressources appartenant
                  à d'autres Agents, et cela prévient la modification des Rôles
                  d'utilisateurs.

                Si vous découvrez un problème avec `INCOMMON::IncommonAgent`, merci de le
                rapporter vers <https://framagit.org/incommon/incommon-api>.
                DESCRIPTION
              },
            },
            uuid: '7218b579-a1b4-4318-9fa8-e8dbff2a61c0'
          )
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
