# frozen_string_literal: true

module INCOMMON
  class << self
    # Return current user's API key, or API_KEY_PUBLIC
    #
    # +current_user+ should respond to +api_key+
    def api_key(current_user = nil)
      current_user&.api_key || API_KEY_PUBLIC
    end

    # Return the root URL for IN COMMON API. (It varies according to +Rails.env+)
    def api_url
      API_ROOT_URL
    end

    # Return the version for API calls
    def api_version
      "v#{VERSION.split('.').first}"
    end

    # Return the full application version
    def version
      VERSION
    end
  end
end
