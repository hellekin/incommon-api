# frozen_string_literal: true

module INCOMMON
  module CurrentUser
    # User can be found from different elements of the current request:
    #
    # - Query string:
    #   - ?api_key=<public api key>        # => AnonymousUser
    #   - ?api_key=<personal api token>    # => Restricted User
    #   - ?auth_token=<private auth token> # => Authenticated User
    # - Authorization header:
    #   - Token <public api key>           # => AnonymousUser
    #   - Token <personal api key>         # => Restricted User
    #   - Token <personal ephemeral token> # => Restricted User
    #   - Bearer <personal auth_token>     # => Authenticated User
    #
    def self.new(request)
      # Ensure we have a user even when there is no request
      return User.anonymous_user unless require_authentication?(request)

      kind, token = parse_authentication(request)

      user = if kind == 'Token'
               unless UUIDParameter::UUID_V4_REGEX.match?(token)
                 raise(INCOMMON::API::Errors::UnauthorizedError,
                       I18n.t('api.help.unauthorized', kind: 'invalid API key'))
               end

               User.find_by(api_token: token)&.restrict!
             else
               user_from_auth_token(token)
             end
      user.presence || raise(INCOMMON::API::Errors::UnauthorizedError,
                             I18n.t('api.help.unauthorized', kind: 'unknown user'))
    end

    class << self
      private

      def user_from_auth_token(_token)
        raise NotImplementedError, I18n.t('api.errors.bearer_token_not_implemented')
        # raise ActiveRecord::RecordNotFound, I18n.t('api.errors.invalid_auth_token')
      end

      def parse_authentication(request)
        # Check the Query string first
        if request.params.key?(:auth_token)
          kind  = 'Bearer'
          token = request.params[:auth_token]
        elsif request.params.key?(:api_key)
          kind  = 'Token'
          token = request.params[:api_key]
        elsif request.headers['Authorization'].present?
          kind, token, garbage = request.headers['Authorization'].split(' ', 3)
          unless garbage.nil?
            raise ArgumentError, I18n.t('api.errors.invalid_authorization_header')
          end
        else
          raise(INCOMMON::API::Errors::UnauthorizedError,
                I18n.t('api.help.unauthorized', kind: 'bad request'))
        end
        [kind, token]
      end

      # Return true is the passed request requires authentication, false otherwise.
      #
      # The algorithm starts from a sane default: always require authentication.
      #
      # Then it checks for conditions where authentication is not needed:
      #
      # - the request object is not an actual request, and won't provide the
      #   necessary conditions for the authentication process to succeed. (See
      #   parse_authentication for such conditions.)
      # - the request path is the root_url ('/')
      #
      # Finally it requires authentication for all unsafe request methods (i.e.,
      # not GET nor HEAD).
      #
      def require_authentication?(request)
        # Require authentication by default
        required = true
        # Skip authentication if the request does not provide the required interface
        required = false unless request.respond_to?(:params) && request.respond_to?(:headers)
        # Skip authentication if the request is addressed to the root_url
        required = false if     required && request&.path == '/'
        # Require authentication if an API key was passed
        required = true  if     !required && request.params.key?(:api_key)
        # Require authentication if request method is unsafe
        required = true  unless ['GET', 'HEAD', nil].include?(request&.method)

        required
      end
    end
  end
end
