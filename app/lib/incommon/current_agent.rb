# frozen_string_literal: true

module INCOMMON
  # CurrentAgent provides an interface for the current_user object that keeps it
  # into the narrow context of its current Agent. As User roles change from an
  # Agent to the next, this allows to customize the authorization according to
  # the action context of the User.
  #
  # Note that by default, an authenticated User will inherit the default IN
  # COMMON Agent context to which User is always an editor with the capability
  # to create and edit resources in this context. "Unmoderated" resources,
  # therefore, appear in the IN COMMON Agent unless another Agent was provided.
  #
  # @example
  #   def current_user
  #     @current_user ||= INCOMMON::CurrentAgent.new(active_user, params[:agent_id])
  #   end
  #
  #   current_user.editor? # => true
  #   current_user.user    # => <User...>
  #   current_user.leader? # => false
  #   current_user.users   # => []
  #
  class CurrentAgent < SimpleDelegator
    attr_reader :agent, :roles, :user

    def initialize(user, agent_id)
      @agent = Agent.find_by(uuid: agent_id) || Agent.incommon_agent
      @user  = user
      @roles = @agent.roles.find_by(user: user) || @agent.default_roles
    end

    def authenticated?
      user&.authenticated? == true
    end

    # We do not delegate to role since it can be nil
    %w(editor? leader? maintainer? observer?).each do |method|
      define_method(method) do
        roles&.send(method) == true
      end
    end

    def current_agent_is_incommon?
      id == Agent::INCOMMON_AGENT_ID
    end
    alias is_incommon? current_agent_is_incommon?

    # Only provide access to Agent users to the leaders, whose role is to manage
    # them. Other Agent roles will see an empty array.
    def users
      leader? ? super : []
    end

    def __getobj__ # :nodoc:
      @agent
    end
  end

  class IncommonAgent < CurrentAgent
    def initialize
      @agent = Agent.incommon_agent
      @user  = User.anonymous_user
      @roles = @agent.roles.find_by(user: @user)
    end

    def default_roles
      %w(editor)
    end
  end
end
