# frozen_string_literal: true

# Perform Single Sign-On using Discourse
module SSO
  require 'securerandom'
  require_relative '../../config/initializers/sso_config'
  require 'sso/from_discourse'
end
