# frozen_string_literal: true

# == IN COMMON Shared Code Library
#
# This gem contains shared logic between incommon-adm, incommon-api, and other
# applications that want to interact with the IN COMMON API.

require 'digest/sha2'

%w(
  version
  core
  functions
  current_user
  current_agent
  api
  cors
).each do |dep|
  require_dependency "incommon/#{dep}"
end
