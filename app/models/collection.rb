# frozen_string_literal: true

class Collection < ApplicationRecord
  # I18n :translations
  extend Mobility
  translates :name, :summary, :description

  belongs_to :agent, class_name: 'Agent'
  has_many :resource_collections, dependent: :destroy, source: :collection
  with_options through: :resource_collections do
    has_many :resources, source: :resource
  end
  # Break the map, but do not destroy it: we might want to restore it with a new
  # collection, in which case existing maps would still work.
  has_many :maps, dependent: :nullify
end

# == Schema Information
#
# Table name: collections
#
#  id           :bigint(8)        not null, primary key
#  translations :json
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_collections_on_agent_id  (agent_id)
#
