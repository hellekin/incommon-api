# frozen_string_literal: true

class ResourceAddress < ApplicationRecord
  include Bitfields

  bitfield   :flags, :primary, :publicly_accessible, :office, :shipping

  belongs_to :address
  has_one    :main_address,
             -> { where(bitfield_sql(primary: true)) },
             class_name: 'Address',
             inverse_of: :resource_addresses
  belongs_to :resource, polymorphic: true
  belongs_to :agent,
             -> { where(resource_type: 'Agent') },
             class_name: 'Agent',
             inverse_of: :resource_addresses,
             optional: true
end

# == Schema Information
#
# Table name: resource_addresses
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  address_id    :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_addresses_on_address_id                     (address_id)
#  index_resource_addresses_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (address_id => addresses.id)
#
