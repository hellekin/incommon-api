# frozen_string_literal: true

class Section < ApplicationRecord
  # I18n :translations
  extend Mobility
  translates :name, :summary, :description

  belongs_to :category, counter_cache: true
  has_one :taxonomy, through: :category
  has_many :resource_sections, dependent: :destroy
  with_options through: :resource_sections, source: :resource do
    has_many :resources, source_type: 'Resource'
    has_many :entities,  source_type: 'Entity'
    has_many :places,    source_type: 'Place'
    has_many :services,  source_type: 'Service'
    has_many :things,    source_type: 'Thing'
  end

  acts_as_list column: :rank, scope: :category

  validates :name,
            uniqueness: { scope: :category_id },
            length: 3..64
end

# == Schema Information
#
# Table name: sections
#
#  id              :bigint(8)        not null, primary key
#  color           :string(25)
#  rank            :integer
#  resources_count :integer          default(0)
#  translations    :json
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category_id     :bigint(8)
#
# Indexes
#
#  index_sections_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
