# frozen_string_literal: true

class Thing < Resource; end

# == Schema Information
#
# Table name: resources
#
#  id           :bigint(8)        not null, primary key
#  meta         :json
#  public       :boolean
#  translations :json
#  type         :string(16)
#  uuid         :uuid
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_resources_on_agent_id  (agent_id)
#
