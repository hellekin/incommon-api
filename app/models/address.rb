# frozen_string_literal: true

class Address < ApplicationRecord
  has_many :resource_addresses, source: :resource, dependent: :destroy
  with_options through: :resource_addresses, source: :resource do
    has_many :resources, source_type: 'Resource'
    has_many :entities,  source_type: 'Entity'
    has_many :places,    source_type: 'Place'
    has_many :services,  source_type: 'Service'
    has_many :things,    source_type: 'Thing'
  end

  before_validation :normalize_country

  validates :country,
            presence: true,
            length: { is: 2 }

  private

  def normalize_country
    country.upcase!
  end
end

# == Schema Information
#
# Table name: addresses
#
#  id          :bigint(8)        not null, primary key
#  country     :string(2)
#  locality    :string
#  pobox       :string
#  postal_code :string(16)
#  region      :string
#  street      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
