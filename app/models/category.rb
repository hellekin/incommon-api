# frozen_string_literal: true

class Category < ApplicationRecord
  # I18n :translations
  extend Mobility
  translates :name, :summary, :description

  belongs_to :taxonomy, counter_cache: true
  has_many :sections,
           -> { order(rank: :asc) },
           dependent: :destroy,
           inverse_of: :category

  acts_as_list column: :rank, scope: :taxonomy

  validates :name,
            presence: true,
            uniqueness: { scope: :taxonomy_id },
            length: 3..64
end

# == Schema Information
#
# Table name: categories
#
#  id             :bigint(8)        not null, primary key
#  color          :string(25)
#  rank           :integer
#  sections_count :integer          default(0)
#  translations   :json
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  taxonomy_id    :bigint(8)
#
# Indexes
#
#  index_categories_on_taxonomy_id  (taxonomy_id)
#
# Foreign Keys
#
#  fk_rails_...  (taxonomy_id => taxonomies.id)
#
