# frozen_string_literal: true

class ResourceEmail < ApplicationRecord
  include Bitfields

  bitfield :flags, :primary, :validated, :personal

  belongs_to :email
  belongs_to :resource, polymorphic: true
  with_options inverse_of: :resource_emails do
    has_one :main_email, -> { where(bitfield_sql(primary: true)) }, class_name: 'Email'
    belongs_to :agent, -> { where(resource_type: 'Agent') }, class_name: 'Agent', optional: true
  end
end

# == Schema Information
#
# Table name: resource_emails
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  email_id      :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_emails_on_email_id                       (email_id)
#  index_resource_emails_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (email_id => emails.id)
#
