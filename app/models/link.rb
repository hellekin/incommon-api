# frozen_string_literal: true

class Link < ApplicationRecord
  include Bitfields

  SECURE_SCHEMES = %w(https ircs nntps).freeze

  bitfield :flags, :secure, :not_found, :cert_error, :server_error

  def failing?
    not_found? || server_error? || secure? && cert_error?
  end

  def insecure?
    !secure?
  end

  has_many :resource_links, dependent: :destroy
  has_many :agents, through: :resource_links, foreign_key: :resource_id, foreign_type: 'Agent'

  before_save :set_flags
  # after_save :check_service # TODO: add background job to check link validity

  validates :uri,
            presence: true,
            format: { with: URI::ABS_URI }

  class << self
    def with_uri!(uri)
      find_or_create_by!(uri: uri)
    end
  end

  private

  def set_flags
    set_bitfield_value(:secure, SECURE_SCHEMES.include?(URI.parse(uri).scheme))
  end
end

# == Schema Information
#
# Table name: links
#
#  id         :bigint(8)        not null, primary key
#  flags      :integer          default(0), not null
#  uri        :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
