# frozen_string_literal: true

class Resource < ApplicationRecord
  # I18n :translations
  extend Mobility
  translates :name, :summary, :description
  # Universally Unique Identifier :uuid
  include UUIDParameter

  belongs_to :agent, optional: true, class_name: 'Agent', inverse_of: :resources

  with_options dependent: :destroy do
    has_many :locations, as: :locatable
    has_many :resource_addresses
    has_many :resource_emails
    has_many :resource_links
    has_many :resource_phones
  end

  has_many :addresses, through: :resource_addresses, source: :address
  has_many :emails,    through: :resource_emails,    source: :email
  has_many :links,     through: :resource_links,     source: :link
  has_many :phones,    through: :resource_phones,    source: :phone

  with_options inverse_of: :resource do
    has_one  :primary_address, ->(res) { res.primary }, class_name: 'ResourceAddress'
    has_one  :primary_email,   ->(res) { res.primary }, class_name: 'ResourceEmail'
    has_one  :primary_link,    ->(res) { res.primary }, class_name: 'ResourceLink'
    has_one  :primary_phone,   ->(res) { res.primary }, class_name: 'ResourcePhone'
  end

  has_one  :main_address, through: :primary_address, class_name: 'Address'
  has_one  :main_email,   through: :primary_email,   class_name: 'Email'
  has_one  :main_link,    through: :primary_link,    class_name: 'Link'
  has_one  :main_phone,   through: :primary_phone,   class_name: 'Phone'

  has_many :resource_collections, dependent: :destroy

  has_many :resource_sections, dependent: :destroy
  has_many :sections, through: :resource_sections, source: :section
end

# == Schema Information
#
# Table name: resources
#
#  id           :bigint(8)        not null, primary key
#  meta         :json
#  public       :boolean
#  translations :json
#  type         :string(16)
#  uuid         :uuid
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_resources_on_agent_id  (agent_id)
#
