# frozen_string_literal: true

module EmailHelper
  def hash_email(address)
    Digest::SHA256.hexdigest(address.to_s)
  end
  module_function :hash_email
end
