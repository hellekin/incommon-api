# frozen_string_literal: true

class MapPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    record_public?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
