# frozen_string_literal: true

CorsOriginsPolicy = Struct.new(:user, :origin)

class CorsOriginsPolicy
  attr_reader :user, :origin

  def initialize(user, origin)
    @user = user
    @origin = origin
  end

  def index?
    true
  end

  def show?
    true
    # user.agent.origins.where(url: URI.parse(origin)).exists?
  end

  def create?
    user.admin?
  end

  def update?
    user.admin?
  end

  def delete?
    user.admin?
  end
end
