# frozen_string_literal: true

class ResourcePolicy < ApplicationPolicy
  def index?
    record_public?
  end

  def show?
    record_public?
  end

  def create?
    user.editor?
  end

  def update?
    create?
  end

  def destroy?
    user.maintainer?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
