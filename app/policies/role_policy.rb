# frozen_string_literal: true

class RolePolicy < ApplicationPolicy
  def index?
    user&.leader?
  end

  def show?
    user&.leader? || @record.user == user&.user
  end

  def create?
    false
  end

  def update?
    false
  end

  def destroy?
    false
  end

  class Scope < Scope
    def resolve
      if user&.leader? == true
        if user&.is_incommon? == true
          scope.all
        else
          scope.where(agent_id: user.uuid)
        end
      else
        scope.where(user_id: user.user.uuid)
      end
    end
  end
end
