# frozen_string_literal: true

class AgentPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    record == user.agent || record.public?
  end

  def create?
    user.editor?
  end

  def update?
    create?
  end

  def destroy?
    user.maintainer?
  end

  class Scope < Scope
    def resolve
      if user.current_agent_is_incommon? || user.leader? || user.maintainer?
        scope.all
      else
        scope.where(public: true)
      end
    end
  end
end
