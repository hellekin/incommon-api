# frozen_string_literal: true

class CollectionPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.editor?
  end

  def update?
    create?
  end

  def destroy?
    user.maintainer?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
