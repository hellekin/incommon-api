# INCOMMON Database Setup

The `incommon-api` database uses [PostgreSQL] 10 with [PostGIS] extensions.

[PostgreSQL]: https://www.postgresql.org/ "The World's Most Advanced Open Source Relational Database"
[PostGIS]: https://postgis.net/ "Spatial and Geographic objects for PostgreSQL"

## Backup & Restore

### Backup

The database structure is saved in `db/structure.sql` when you run `rails db:migrate`.

Data can be saved with:

``` shell
pg_dump --column-insert --data-only -d $DB_NAME | gzip -9 - > db/incommon_data.`date +%F`.sql.gz
```

This should be performed on a regular basis, e.g., daily in a crontab:

```
PATH=/bin:/usr/bin
# Backup IN COMMON database every day at 02:04 AM
4 2 * * * pg_dump --column-insert --data-only -d incommon-api | gzip -9 - > db/incommon_data.`date +%F`.sql.gz
```

### Restore

In order to restore data from a backup, it's better to drop the existing
database and follow the instructions in the Production section below.

In a nutshell, from an existing backup file:

``` shell
zcat db/incommon_data.2018-10-01.sql.gz | psql -d incommon-api -u $INCOMMON_API_DB_USER -p
```

## Development

``` shell
bundle exec rails db:setup
```

### Resetting the database

You can restore the database to its pristine state, or recreate it from scratch
if you have new migrations. If you do not have new migrations, you could instead
run `bundle exec rails db:reset`, as it will recreate the database not from
migrations but from the `db/structure.sql` file.

``` shell
bundle exec rails db:drop
bundle exec rails db:setup
bundle exec rails db:migrate
```

## Production

This must be run once when first deploying the `incommon-api`: it will create
the database and user role, then import the raw data from a backup: here
`2018-10-01` is an example date! Copy the right filename from `db/` instead.

``` shell
INCOMMON_API_DB_USER=someuser INCOMMON_API_DB_PASS=somestrongsecret RAILS_ENV=production \
  bundle exec rails db:setup
zcat db/incommon_data.2018-10-01.sql.gz | psql -d incommon-api -u $INCOMMON_API_DB_USER -p
```
