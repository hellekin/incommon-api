# frozen_string_literal: true

require_relative '../../config/application'
require_relative '../../config/environment'

Mobility.locale = :en

if taxonomy = Taxonomy.i18n.find_by(name: 'Dewey Maps')
  raise ArgumentError, 'Dewey Maps Taxonomy already exists: aborted.'
end

require 'json'

ins  = Rails.root.join('doc', 'import', 'dewey-maps-categories-en.json')
out  = Rails.root.join('doc', 'import', 'dewey-maps-sections.json')

cats = JSON.parse(File.read(ins))

## Agent

agent = Agent.i18n.find_by(name: 'Dewey Maps')

if agent.blank?
  Rails.logger.info "Missing Agent 'Dewey Maps': creating it..."
  agent = AgentCreator.create(
    name: 'Dewey Maps', email: 'maps@dewey.be', uri: 'https://maps.dewey.be',
    email_validated: true, uri_is_origin: true
  )
end

Rails.logger.info agent.inspect

unless agent
  raise ArgumentError,
        "Dewey Maps Agent cannot be created!\n#{agent&.errors&.inspect}\n#{e.message}"
end

# Taxonomy.create!(name: 'Dewey Maps', agent_id: agent.id, summary: 'The original IN COMMON classification.')
dewey = agent.taxonomies.create!(
  name: 'Dewey Maps',
  summary: 'The original IN COMMON classification.'
)

subcat = {}

cats.each do |entry|
  mycat = dewey.categories.create!(name: entry['name'], color: "##{entry['color']}")
  entry['subcategories'].each do |sub|
    sec = mycat.sections.i18n.find_by(name: sub['name']) ||
          mycat.sections.create!(name: sub['name'], color: "##{entry['color']}")
    subcat[sub['id']] = sec.id unless subcat.key?(sub['id'])
  end
end

File.open(out, 'w+') { |f| f.puts JSON.dump(subcat) }
