# coding: utf-8
# frozen_string_literal: true

require_relative '../../config/application'
require_relative '../../config/environment'

require 'json'

agent = Agent.find_by(name: 'Réseau de Consommateurs Responsables')
if agent.blank?
  Rails.logger.info 'Creating RCR Agent'
  agent = AgentCreator.create(name: 'Réseau de Consommateurs Responsables',
                              email: 'info@asblrcr.be',
                              uri: 'https://www.asblrcr.be',
                              email_validated: true,
                              uri_is_origin: true
                             )
end

raise ArgumentError, "RCR Agent cannot be created!\n#{agent&.errors&.inspect}" unless agent

Rails.logger.info "Using RCR Agent: #{agent.uuid}"

# Ensure we don't create it twice
x = Taxonomy.find_by(name: 'RCR')
if x.present?
  Rails.logger.info "Taxonomy #{x.uuid} already exists. Aborted."
  return false
end

Rails.logger.info 'Creating RCR Taxonomy'
rcr = Taxonomy.create!(name: 'RCR', agent_id: agent.id,
                       summary: 'Classification du Réseau de Consommateurs Responsables.',
                       uuid: '84691a6d-9d41-48bd-8299-a4611a85f7ae')
Rails.logger.info "Using RCR Taxonomy: #{rcr.uuid}"

c1 = Category.create!(name: 'Donnerie', taxonomy_id: rcr.id)
Section.create!(name: 'Donnerie physique', category_id: c1.id)
Section.create!(name: 'Donnerie virtuelle', category_id: c1.id)
Rails.logger.info("Created Category #{c1.name} with #{c1.sections.count} sections")

c2 = Category.create!(name: 'Initiative citoyenne', taxonomy_id: rcr.id)
['Groupement d\'Achat Coopératif (GAA)', 'Repair Café', 'RES',
 'Système d\Échange Local (SEL)', 'Autres initiatives'].each do |section|
  Section.create!(name: section, category_id: c2.id)
end
Rails.logger.info("Created Category #{c2.name} with #{c2.sections.count} sections")

c3 = Category.create!(name: 'Potager collectif', taxonomy_id: rcr.id)
['Potager citoyen', 'Potager pédagogique', 'Potager scolaire',
 'Centre Public d\'Aide Sociale (CPAS)', 'EFT'].each do |section|
  Section.create!(name: section, category_id: c3.id)
end
Rails.logger.info("Created Category #{c3.name} with #{c3.sections.count} sections")
