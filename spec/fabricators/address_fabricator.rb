Fabricator(:address) do
  street      "Avenue Louis Bertrand, 28"
  pobox       nil
  postal_code "1030"
  locality    "Schaerbeek"
  region      "Bruxelles Capitale"
  country     "BE"
end

# == Schema Information
#
# Table name: addresses
#
#  id          :bigint(8)        not null, primary key
#  country     :string(2)
#  locality    :string
#  pobox       :string
#  postal_code :string(16)
#  region      :string
#  street      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
