Fabricator(:taxonomy) do
  name        { Faker::Name.name[0...64] }
  summary     { Faker::Lorem.sentence[0...136] }
  description { Faker::Markdown.sandwich(6, 3) }
  uuid        { SecureRandom.uuid }
  agent
end

# == Schema Information
#
# Table name: taxonomies
#
#  id               :bigint(8)        not null, primary key
#  categories_count :integer          default(0)
#  translations     :json
#  uuid             :string(36)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  agent_id         :bigint(8)
#
# Indexes
#
#  index_taxonomies_on_agent_id  (agent_id)
#  index_taxonomies_on_uuid      (uuid) UNIQUE
#
