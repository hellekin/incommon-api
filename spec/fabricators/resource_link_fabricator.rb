Fabricator(:resource_link) do
  link     
  resource 
  flags    1
end

# == Schema Information
#
# Table name: resource_links
#
#  id            :bigint(8)        not null, primary key
#  flags         :integer          default(0), not null
#  resource_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  link_id       :bigint(8)
#  resource_id   :bigint(8)
#
# Indexes
#
#  index_resource_links_on_link_id                        (link_id)
#  index_resource_links_on_resource_type_and_resource_id  (resource_type,resource_id)
#
# Foreign Keys
#
#  fk_rails_...  (link_id => links.id)
#
