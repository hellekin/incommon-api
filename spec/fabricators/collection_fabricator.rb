Fabricator(:collection) do
  name        "MyString"
  summary     "MyString"
  description "MyText"
  agent       nil
end

# == Schema Information
#
# Table name: collections
#
#  id           :bigint(8)        not null, primary key
#  translations :json
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  agent_id     :bigint(8)
#
# Indexes
#
#  index_collections_on_agent_id  (agent_id)
#
