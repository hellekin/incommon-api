Fabricator(:section) do
  name        { Faker::Dune.character }
  summary     { Faker::Dune.saying("fremen") }
  description { Faker::Markdown.sandwich(6, 3) }
  color       { Faker::Color.hex_color }
  category
end

# == Schema Information
#
# Table name: sections
#
#  id              :bigint(8)        not null, primary key
#  color           :string(25)
#  rank            :integer
#  resources_count :integer          default(0)
#  translations    :json
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  category_id     :bigint(8)
#
# Indexes
#
#  index_sections_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#
