Fabricator(:user) do
  username   "MyString"
  password   ""
  auth_token "MyString"
  nonce      "MyString"
  api_token  "MyString"
end

# == Schema Information
#
# Table name: users
#
#  id              :bigint(8)        not null, primary key
#  api_token       :uuid
#  auth_token      :string
#  password_digest :string
#  sha256_hash     :string(64)
#  token           :string
#  username        :string
#  uuid            :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
