require 'rails_helper'

RSpec.describe Position, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end

# == Schema Information
#
# Table name: positions
#
#  id         :bigint(8)        not null, primary key
#  box        :json
#  elevation  :decimal(7, 2)
#  geo_type   :string
#  geometry   :geometry({:srid= geometry, 0
#  latitude   :decimal(9, 7)
#  longitude  :decimal(10, 7)
#  radius     :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_positions_on_geometry  (geometry) USING gist
#
