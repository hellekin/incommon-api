if Rails.env.development?
  require "byebug"
  require "pry-byebug"
  
  # Byebug.start_server 'localhost', ENV.fetch("BYEBUG_SERVER_PORT", 1048).to_i
end
