# frozen_string_literal: true

# Set default host for url_helpers
Rails.application.routes.default_url_options = {
  host: URI.parse(INCOMMON::API_ROOT_URL).host
}
