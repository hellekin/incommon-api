# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

gem 'rails', '~> 5.2.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rgeo'
gem 'activerecord-postgis-adapter'

gem 'bootsnap', '>= 1.1.0', require: false, platform: :mri

gem 'acts_as_list'
gem 'bcrypt', '~> 3.1.7'
gem 'bitfields'
gem 'faker', '~> 1.9.0'
gem 'fast_blank', platform: :mri
gem 'iana-data', require: 'iana'
gem 'jsonapi-rails'
gem 'mobility'
gem 'phony_rails'
gem 'pundit'
gem 'rack-cors', require: 'rack/cors'
gem 'rack-protection'
gem 'uuid_parameter'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'fabrication'
  gem 'guard'
  gem 'pry'
  gem 'pry-byebug'
  gem 'rb-inotify', '~> 0.9'
  gem 'rspec'
  gem 'rspec-rails', '~> 3.7'
  gem 'shoulda', require: false
  gem 'shoulda-matchers'
end

group :development do
  gem 'annotate'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'fasterer', require: false
  gem 'guard-brakeman'
  gem 'guard-preek'
  gem 'guard-rspec'
  gem 'guard-rubocop'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'pry-rails'
  gem 'rails-erd'
  gem 'rubycritic', require: false
  gem 'seed_dump'
  gem 'spring'
end

group :test do
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'simplecov', require: false
end
